import shutil


def copyDirectory(src, dest):
    try:
        shutil.copytree(src, dest)
    except shutil.Error as e:
        print('Directory not copied. Error: %s' % e)
    except OSError as e:
        print('Directory not copied. Error: %s' % e)


if __name__ == '__main__':
    copyDirectory('etc/kamailio_example', 'etc/kamailio')
