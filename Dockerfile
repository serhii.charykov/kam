FROM kamailio/kamailio-ci:5.2-alpine

RUN apk add --no-cache mysql-client && \
    rm -rf /var/cache/apk/*

VOLUME /etc/kamailio
